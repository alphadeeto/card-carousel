import React from 'react';
import PropTypes from 'prop-types';
import { PlusCircleOutlined } from '@ant-design/icons';
import './styles.css';

function Card(props) {

  const { label, onClick } = props

  return (
    <button 
      className="add-btn clickable"
      onClick={onClick}
      >
      <PlusCircleOutlined className='add-icon'/>
      <div>{label}</div>
    </button>
  )
}

Card.propTypes = {
  details: PropTypes.object,
  onClick: PropTypes.func,
}

export default Card;