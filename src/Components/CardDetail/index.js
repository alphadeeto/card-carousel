import React from 'react';
import PropTypes from 'prop-types';
import Card from '../Card';
import './styles.css';

function CardDetail(props) {

  const { details = {} } = props

  return (
    <div className="detail-container">
      <Card details={details}/>
      <h3>Customer Detail</h3> 
      <div className="detail-content">
        <div className="label">Customer Name</div>
        <div>{details.card_name}</div>
        <div className="label">Card Number</div>
        <div>**** **** **** {details.card_last_four}</div>
        <div className="label">Expiry Date</div>
        <div>{details.expiry}</div>
      </div>
    </div>
  )
}

CardDetail.propTypes = {
  details: PropTypes.object,
}

export default CardDetail;