import React from 'react';
import PropTypes from 'prop-types';
import './styles.css';

function AddButton(props) {

  const { details = {}, onClick } = props

  return (
    <div
      className={`card ${details.color} ${onClick && 'clickable'}`}
      onClick={onClick}
    >
      <div>{details.card_name}</div>
      <div>**** **** **** {details.card_last_four}</div>
      <div>EXPIRY</div>
      <div>{details.expiry}</div>
    </div>
  )
}

AddButton.propTypes = {
  details: PropTypes.object,
  onClick: PropTypes.func,
}

export default AddButton;