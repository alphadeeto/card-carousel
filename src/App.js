import React, { useState } from 'react';
import { Carousel, Modal } from 'antd';
import { RightOutlined, LeftOutlined } from '@ant-design/icons';
import Card from './Components/Card';
import CardDetail from './Components/CardDetail';
import AddButton from './Components/AddButton';
import './App.css';

function App() {
  const [cards] = useState([
    {
      "card_name": "Superhuman", 
      "card_last_four": "5139", 
      "expiry": "03/2026",
      "color": "red"
    },
    {
      "card_name": "Bruce Wayne", 
      "card_last_four": "4214", 
      "expiry": "08/2025",
      "color": "red"
    },
    {
      "card_name": "Clark Kent", 
      "card_last_four": "6433", 
      "expiry": "03/2024",
      "color": "blue"
    },
    {
      "card_name": "Barry Allen", 
      "card_last_four": "3245", 
      "expiry": "09/2026",
      "color": "blue"
    },
    {
      "card_name": "Arthur Curry", 
      "card_last_four": "7655", 
      "expiry": "04/2026",
    },
  ]);
  const [isDetailVisible, setIsDetailVisible] = useState(false);
  const [selectedCard, setSelectedCard] = useState({});

  const showDetail = (item) => {
    setIsDetailVisible(true);
    setSelectedCard(item);
  }

  const hideDetail = () => {
    setIsDetailVisible(false);
    setSelectedCard({});
  }

  const addNewCard = () => {
    Modal.info({
      title: 'Adding New Card',
      content: (
        <div>
          You are about to add a new card.
        </div>
      ),
      onOk() {},
    });
  }

  return (
    <div className="App">
      <div className="container">
        <AddButton
          onClick={addNewCard}
          label="Add new card"/>
        <Carousel 
          arrows
          nextArrow={<RightOutlined/>} 
          prevArrow={<LeftOutlined/>}
          slidesToShow={4}
          slidesToScroll={1}
          dots={true}
          dotPosition="top"
          dotsClass="carousel-dots"
          >
          {cards.map(item => (
            <Card
              onClick={() => {showDetail(item)}}
              details={item}/>
          ))}
        </Carousel>
      </div>
      <Modal
        visible={isDetailVisible}
        onCancel={hideDetail}
        footer={null}
        >
          <CardDetail details={selectedCard}/>
      </Modal>
    </div>
  );
}

export default App;
